﻿#include <iostream>
#include <math.h>
#include <fstream>
#include <string>

using namespace std;

inline double f(double s, double nu)
{
	return s / (s + (1. - s) * nu);
}
inline double df(double s, double nu)
{
	return nu / pow(((s - 1) * nu - s), 2);
}

double gu(double x)
{
	if (x == 0) return 1;
	else return 0;

}

double fvsp(double u, double m, double M)
{
	if (m <= u && u <= M)
	{
		return u;
	}
	else if (u < m)
	{
		return m;
	}
	else if (u > M)
	{
		return M;
	}
}

void modKabare(const double r, const double t_lim, const double x_lim, const double n_points, const double nu, const double m, const double w)
{

	double t_bezr = w * t_lim / m / x_lim;

	double h = 1. / (n_points - 1);

	double* sPrev = new double[n_points];
	double* sCurr = new double[n_points];

	double* sPrev_polushag = new double[n_points];
	double* sCurr_polushag = new double[n_points];



	//граничные условия
	for (int i = 0; i < n_points; i++)
	{
		sPrev[i] = gu(i * h);
		sPrev_polushag[i] = (sPrev[i] + gu((i + 1) * h)) / 2;
	}

	double maxDf = 0;
	for (int i = 0; i < n_points - 1; i++)
	{
		if (maxDf < df(sPrev_polushag[i], nu))
		{
			maxDf = df(sPrev_polushag[i], nu);
		}
	}
	double tau = r * h / maxDf;

	//первый шаг
	for (int i = 0; i < n_points - 1; i++)
	{
		sCurr_polushag[i] = sPrev_polushag[i] - tau / h / 2 * (f(sPrev[i + 1], nu) - f(sPrev[i], nu));
	}


	for (double t = 0; t <= t_bezr; t += tau)
	{
		sCurr[0] = 1;
		for (int i = 1; i < n_points; i++)
		{
			double u = 2. * sCurr_polushag[i - 1] - sPrev[i - 1];
			double m, M;
			if (sPrev[i] < sCurr_polushag[i - 1])
			{
				m = sPrev[i];
				M = sCurr_polushag[i - 1];
			}
			else
			{
				m = sCurr_polushag[i - 1];
				M = sPrev[i];
			}
			sCurr[i] = fvsp(u, m, M);
		}
		for (int i = 0; i < n_points; i++)
		{
			sPrev[i] = sCurr[i];
			sPrev_polushag[i] = sCurr_polushag[i];
		}

		for (int i = 0; i < n_points - 1; i++)
		{
			sCurr_polushag[i] = sPrev_polushag[i] - tau / h * (f(sPrev[i + 1], nu) - f(sPrev[i], nu));
		}
	}
	string filename = "rezult.txt";
	ofstream fout(filename);

	for (int x = 0; x < n_points; x++)
	{
		fout << x * h * x_lim << '\t' << sCurr[x] << endl;
	}

	delete[] sCurr;
	delete[] sPrev;
	delete[] sPrev_polushag;
	delete[] sCurr_polushag;
	fout.close();
}



int main()
{
	setlocale(0, "ru_ru");
	double r = 0.5,
		t_lim = 4,
		x_lim = 10,
		n_point = 100,
		nu = 0.5,
		m = 0.3,
		w = 0.34;

	modKabare(r, t_lim, x_lim, n_point, nu, m, w);
}


